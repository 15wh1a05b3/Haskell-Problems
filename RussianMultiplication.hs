import Prelude hiding (odd)

half :: Int -> Int
half = (`div` 2)
 
double :: Int -> Int
double x = 2 * x

odd :: Int -> Bool
odd = (== 1) . (`mod` 2)

russianMultiplication :: Int -> Int -> Int
russianMultiplication a b =
  sum $
  map snd $
  filter (odd . fst) $
  zip (takeWhile (>= 1) $ iterate half a) (iterate double b)
 
main :: IO ()
main = print $ russianMultiplication 12 56

