import Data.List (sort)
import Data.List (permutations)

main :: IO ()
main = putStrLn $ (sort $ permutations ['0'..'9']) !! 999999
