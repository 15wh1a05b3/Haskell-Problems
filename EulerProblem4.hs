isPalindrome ::  Integer -> Bool
isPalindrome a = show a == reverse (show a)

main ::  IO ()
main = print $ maximum [x | y <- [100..999], z <- [y..999], let x = y * z, isPalindrome x]
