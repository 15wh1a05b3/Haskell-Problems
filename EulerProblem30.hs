digits ::  Integer -> [Integer]
digits 0 = []
digits n = r : digits q where (q, r) = quotRem n 10

main :: IO()
main = print $ sum [n | n <- [1000..1000000], sum (map (^5) (digits n)) == n]
