main = do xs <- fmap (map read . lines) (readFile "digits.txt")
          print . take 10 . show . sum $ xs
